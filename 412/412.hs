import Data.List (transpose, permutations)
import System.Environment


type Gnomon = [[Int]]
type M = Int
type N = Int

checkList :: [Int] -> Int
checkList = foldl1 compareItems 

checkList' :: [Int] -> Int
checkList' = foldl1 compareItems'

compareItems :: Int -> Int -> Int
compareItems 0 _ = 0
compareItems x y
    | x >= y    = y
    | otherwise = 0

compareItems' :: Int -> Int -> Int
compareItems' 0 _ = 0
compareItems' x y
    | x <= y    = y
    | otherwise = 0

-- Check a given Gnomon, to see if the properties hold
-- Does not check whether it is filled with a sequence of integers,
-- only that a square is less than the square below it and to the left
check :: Gnomon -> Bool
check x = checkNormal /= 0 && checkTranspose /= 0
    where
      checkNormal = product $ map checkList x
      checkTranspose =  product $ map checkList' $ transpose x

-- Given a list and the values of m and n, properly creates sublists to
-- represent the gnomon
build :: M -> N -> N -> [Int] -> Gnomon
build _ _ _ [] = []
build m n 0 xs = take m xs : build m n 0 larger
    where
      larger = drop m xs
build m n r xs = take diff xs : build m n (r - 1) smaller
    where
      diff = m - n
      smaller = drop diff xs


-- Generates and properly creates gnomons given m and n
generate :: M -> N -> [Gnomon]
generate m n 
    | m <= n = error "n must be less than m"
    | otherwise = map builder $ permutations [1..total]
      where
        total = m ^ 2 - n ^ 2
        builder = build m n n

main :: IO ()
main = do
    x:y:_ <- getArgs
    let m = read x :: Int
    let n = read y :: Int
    print . length $ [z | z <- generate m n, check z]
